import boto3
import requests

server = 'http://staging.suedostschweiz.ch.c.suedostschweiz.dev.ent.platform.sh'
path = '/so_simplenews/bounce'

def som_newsletter_bounce_handler(event, context):
  r = requests.post(server + path, json=event)
