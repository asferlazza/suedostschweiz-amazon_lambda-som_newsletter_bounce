# What is Lambda?
[More at](http://docs.aws.amazon.com/lambda/latest/dg/intro-core-components.html)

Two core components:

* Event Source (publishes an event)
* Lambda function (processes the event)

Event Source: Can basically be anything, usually another aws service like SES.
Lambda function: A function written in Python or NodeJS containing the logic.

Lambda consists of:

* custom code logic
* dependencies (libraries, packages, whatever)
* configuration (specified in lambda console)
  * compute resources
  * timeout
  * IAM role
  * handler (name of function to execute on invocation)
    * "AWS Lambda provides event data as input to this handler."

# Amazon SES Input Format
[Mora at](http://docs.aws.amazon.com/ses/latest/DeveloperGuide/receiving-email-action-lambda.html)

The input of the lambda functino invoked by a ses event looks as follows:
(This is what the **first parameter** contains)
```
{
   "Records": [
      {
        "eventSource": "aws:ses",
        "eventVersion": "1.0",
        "ses": {
           "receipt": {
               <same contents as SNS notification>
            },
           "mail": {
               <same contents as SNS notification>
           }
         }
     }
   ]
}
```
(This is what the **second parameter** contains)
Context Object: A JSON Object containing context information about the lambda,
such as the remaining execution time available.

# Python Lambda
[More at](http://docs.aws.amazon.com/lambda/latest/dg/python-programming-model.html)

The lambda has the following pattern:
```
def handler_name(event, context):
  ...
```

## Upload Python Lambda
Pack the file and dependencies as .zip.
**Attention**: Do not pack the containing folder!